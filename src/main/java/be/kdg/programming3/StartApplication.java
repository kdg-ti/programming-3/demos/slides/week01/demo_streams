package be.kdg.programming3;

import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class StartApplication {
    public static void main(String[] args) {
        //List<Article> articles =
                Stream.generate(() -> {
                    Random r = new Random();
                    String randomName = "jos" + r.nextInt(100);
                    double randomPrice = r.nextDouble(10, 500);
                    return new Article(randomName, randomPrice);
                })
                .filter(article -> article.getPrice() < 100)
                //.map(article -> article.getName())
                .limit(10)
                //.forEach(name -> System.out.println(name));
                .forEach(System.out::println);
                //.toList();
        //System.out.println(articles);

    }
}
